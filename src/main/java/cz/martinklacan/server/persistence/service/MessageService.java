package cz.martinklacan.server.persistence.service;

import cz.martinklacan.netmessage.dto.MessageDTO;
import cz.martinklacan.netmessage.dto.ThreadInformationDTO;
import cz.martinklacan.server.persistence.entity.Message;
import cz.martinklacan.server.persistence.entity.ThreadInformation;
import cz.martinklacan.server.persistence.repository.MessageRepository;
import cz.martinklacan.server.persistence.repository.ThreadInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessageService {

    private final MessageRepository messageRepository;

    private final ThreadInformationRepository threadInformationRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository, ThreadInformationRepository threadInformationRepository) {
        this.messageRepository = messageRepository;
        this.threadInformationRepository = threadInformationRepository;
    }

    public void create(MessageDTO messageDTO) {

        Message message = new Message(
                messageDTO.getUser(),
                messageDTO.getDate());

        List<ThreadInformation> threadInformation = messageDTO.getList()
                .stream()
                .map(t -> threadInformationToEntity(message, t))
                .collect(Collectors.toList());

        message.setThreadInformation(threadInformation);

        messageRepository.save(message);
        threadInformationRepository.saveAll(threadInformation);

    }

    @Transactional
    public List<MessageDTO> findAll() {
        return messageRepository.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }

    @Transactional
    public List<MessageDTO> findMessages(Integer num) {
        return messageRepository.findMessages(num).stream().map(this::toDTO).collect(Collectors.toList());
    }

    private MessageDTO toDTO(Message message) {

        return new MessageDTO(message.getUser(), message.getDate(), threadInformationToDTO(message.getThreadInformation()));
    }

    private List<ThreadInformationDTO> threadInformationToDTO(List<ThreadInformation> threadInformation) {
        return threadInformation.stream().map(this::entityToThreadInformationDTO).collect(Collectors.toList());
    }

    private ThreadInformationDTO entityToThreadInformationDTO(ThreadInformation threadInformation) {

        return new ThreadInformationDTO(threadInformation.getName(), threadInformation.getState(), threadInformation.getTime());
    }

    private ThreadInformation threadInformationToEntity(Message message, ThreadInformationDTO dto) {

        ThreadInformation threadInformation = new ThreadInformation(dto.getName(), dto.getState(), dto.getTime());
        threadInformation.setMessage(message);
        return threadInformation;
    }
}
