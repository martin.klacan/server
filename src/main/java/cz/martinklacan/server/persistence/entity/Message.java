package cz.martinklacan.server.persistence.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="message")
public class Message {

    @Id
    @GeneratedValue
    private int id;

    @Column(name = "computer_user")
    private String user;

    @Column(name = "message_date")
    private Date date;

    @OneToMany(mappedBy = "message")
    private List<ThreadInformation> threadInformation;

    public Message(String user, Date date) {
        this.user = user;
        this.date = date;
    }

    public Message() { }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<ThreadInformation> getThreadInformation() {
        return threadInformation;
    }

    public void setThreadInformation(List<ThreadInformation> threadInformation) {
        this.threadInformation = threadInformation;
    }
}
