package cz.martinklacan.server.persistence.entity;

import javax.persistence.*;

@Entity
@Table(name="thread_information")
public class ThreadInformation {

    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    @JoinColumn(name = "message_id", nullable = false)
    private Message message;

    private String name;
    private String state;

    private Long time;

    public ThreadInformation() { }

    public ThreadInformation(String name, String state, Long time) {
        this.name = name;
        this.state = state;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getState() {
        return state;
    }

    public Long getTime() {
        return time;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
