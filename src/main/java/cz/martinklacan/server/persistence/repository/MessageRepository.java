package cz.martinklacan.server.persistence.repository;

import cz.martinklacan.server.persistence.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {

    @Query("SELECT m FROM Message m WHERE m.threadInformation.size = :num")
    List<Message> findMessages(Integer num);
}
