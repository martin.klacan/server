package cz.martinklacan.server.persistence.repository;

import cz.martinklacan.server.persistence.entity.ThreadInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ThreadInformationRepository extends JpaRepository<ThreadInformation, Integer> {
}
