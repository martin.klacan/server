package cz.martinklacan.server.rest;

import cz.martinklacan.netmessage.dto.MessageDTO;
import cz.martinklacan.server.persistence.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RestEndpoint {

    private final MessageService messageService;

    @Autowired
    public RestEndpoint(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/messages")
    public List<MessageDTO> all() {
        return messageService.findAll();
    }

    @GetMapping(value = "/findMessages/{num}")
    public List<MessageDTO> sort(@PathVariable Integer num) {
        return messageService.findMessages(num);
    }
}
