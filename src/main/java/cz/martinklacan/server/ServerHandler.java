package cz.martinklacan.server;

import cz.martinklacan.netmessage.dto.MessageDTO;
import cz.martinklacan.server.persistence.service.MessageService;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
@ChannelHandler.Sharable
public class ServerHandler extends ChannelInboundHandlerAdapter {

    private final MessageService messageService;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {

        MessageDTO messageDTO = (MessageDTO) msg;
        log.info(messageDTO.getDate() + "  Message from: " + messageDTO.getUser() + " , num threads: " + messageDTO.getList().size());
        messageService.create(messageDTO);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        log.error(cause.getMessage(), cause);
    }
}
