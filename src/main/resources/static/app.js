var model = {
    messages: []
};

function messageClicked(message) {

    renderDetails(message);
}

function loadMessages() {

    loadFromServer("/messages").then((response) => {

        model.messages = fromJSON(response);
        renderMessages(model.messages);
    });
}

function findMessages() {

    let value = document.getElementById("threadNum").value;
    if (!value || isNaN(parseInt(value, 10)))
        alert("No value was set!");

    loadFromServer("/findMessages/" + value).then((response) => {

        model.messages = fromJSON(response);
        renderMessages(model.messages);
    });
}

function renderMessages(messages) {

    let msgData = document.getElementById("messageData");
    let detailsData = document.getElementById("detailsData");
    msgData.innerHTML = "";
    detailsData.innerHTML = "";

    let table = createTable(["User", "Date/Time"], "tableData");

    messages.forEach(m => {

        let row = createTableRow([m.user, m.date]);
        row.onclick = function () {
            messageClicked(m);
        }
        table.appendChild(row);
    });

    msgData.appendChild(table);
}

function renderDetails(message) {

    let detailsData = document.getElementById("detailsData");
    detailsData.innerHTML = "";

    let table = createTable(["Name", "State", "Time"], "tableData");

    message.list.forEach(d => {

        let row = createTableRow([d.name, d.state, d.time]);
        table.appendChild(row);
    });

    detailsData.appendChild(table);
}

function createTable(headers, cssClass) {

    var table = document.createElement("TABLE");
    table.className = cssClass;

    var row = document.createElement("TR");
    table.appendChild(row);

    headers.forEach(h => {

        var col = document.createElement("TH");
        var txt = document.createTextNode(h);
        col.appendChild(txt);

        row.appendChild(col);
    });
    return table;
}

function createTableRow(columns) {

    var row = document.createElement("TR");

    columns.forEach(c => {

        var col = document.createElement("TD");
        var txt = document.createTextNode(c);
        col.appendChild(txt);

        row.appendChild(col);
    });
    return row;
}

function fromJSON(text) {
    return JSON.parse(text);
}

function loadFromServer(methodName) {

    return new Promise((resolve, reject) => {

        var xhr = new XMLHttpRequest();

        xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                var status = xhr.status;
                if (status === 0 || (status >= 200 && status < 400)) {
                    resolve(xhr.responseText);
                } else {
                    reject();
                }
            }
        };

        xhr.open("GET", methodName);
        xhr.send();
    });
}